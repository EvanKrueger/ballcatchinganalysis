#from __future__ import division
​import PerformParser as pp
import pandas as 
import numpy as np
from scipy import signal as sig
import performFun as pF
​
import bokeh.plotting as bkP
import bokeh.models as bkM
from bokeh.palettes import Spectral6
bkP.output_notebook() 
​
import cv2
import os
import scipy.io as sio
import matplotlib
​
%matplotlib notebook
from ipywidgets import interact
import filterpy as fP
from bokeh.io import push_notebook
​
import Quaternion as qu
​
import matplotlib.pyplot as plt
from scipy.signal import butter, lfilter, freqz
from scipy.fftpack import fft
from mpl_toolkits.mplot3d import Axes3D
​
from bokeh.io import hplot
from bokeh.io import vplot
from bokeh.embed import file_html
from bokeh.resources import CDN
import math
​
​
bkP.output_notebook()
​
#bkP.output_file('timeSeries.html') 
​
#%pylab inline
#%matplotlib notebook
​

fileTime = '2016-3-10-13-49'
#fileTime = '2016-3-8-10-14'
#fileTime = '2016-2-26-20-29'
#fileTime = '2015-12-18-12-7'
#fileTime = '2016-1-29-16-43'
#fileTime = '2016-1-26-15-20'
​
#fileTime = '2015-12-9-20-52'
#fileTime = "2015-11-16-17-31"
expCfgName = "gd_pilot.cfg"
sysCfgName = "PERFORMVR.cfg"
​
filePath = "../Data/exp/" + fileTime + "/"
fileName = "exp_data-" + fileTime
​
expConfig = pF.createExpCfg(filePath + expCfgName)
sysConfig = pF.createSysCfg(filePath + sysCfgName)
​
try:
    sessionDict = pd.read_pickle(filePath + fileName + '.pickle')
​
except:
​
    s1 = pp.readPerformDict(filePath + fileName + ".dict")
    sessionDict = pF.createSecondaryDataframes(s1, expConfig)
    pd.to_pickle(sessionDict, filePath + fileName + '.pickle')
    s1 = []
​
rawDataFrame = sessionDict['raw']
processedDataFrame = sessionDict['processed']
calibDataFrame = sessionDict['calibration']
trialInfoDataFrame = sessionDict['trialInfo']


#processedGBTrial = processedDataFrame.groupby(['trialNumber'])
np.unique(rawDataFrame['trialNumber'].values)#trialID = 79
#trialNum = trialID
#processedGBTrial.get_group(trialNum)['rotatedGazePoint_fr_XYZ'].values[5]
​
gbCalibrationQ = calibDataFrame.groupby(['inCalibrationQ'])
gbTrial = rawDataFrame.groupby(['trialNumber'])
processedGBTrial = processedDataFrame.groupby(['trialNumber'])
​
trialID = 79
trialNum = trialID
gazeVelocity = processedGBTrial.get_group(trialNum)['cycGazeVelocity_fr'].values
#filteredGazeVelocity = sig.medfilt(gazeVelocity, 5)
#gbTrials = s1Processed.groupby('trialNumber')
trialFrames  = gbTrial.get_group(trialNum).index
​
angleList = []
for i in range(len(trialFrames)):
    temp = pF.vectorAngle(processedGBTrial.get_group(trialNum).rotatedGazePoint_fr_XYZ.values[i], processedGBTrial.get_group(trialNum).rotatedBallOnScreen_fr_XYZ.values[i])
    angleList.append(temp)
​
p1 = pF.timeSeries(frametime_fr = gbTrial.get_group(trialNum)['frameTime'].values,
                   yDataList = [gazeVelocity,angleList],
                   events_fr = gbTrial.get_group(trialNum)['eventFlag'].values,
                   yLabel='gaze velocity',
                   yLims = [0,150],
                   legendLabels = ['Gaze Velocity', 'Angular Error'],
                   plotHeight=500)
​
p1.title = 'Filtered cyc gaze velocity'
​
p1.plot_width = 600
​
yLims = [p1.y_range.start,p1.y_range.end]
xLims = [0,p1.plot_width]
​
tBox = bkM.BoxAnnotation(
                         bottom = yLims[0],
                         top = yLims[1],
                         left = np.mean(xLims)-3, left_units='screen',
                         right = np.mean(xLims)+3, right_units='screen',
                         fill_alpha=0.5, fill_color='green', level='overlay')
​
p1.renderers.extend([tBox])
​

plotWidth = 300
​
p2 = bkP.figure(plot_width=plotWidth,plot_height=plotWidth,tools='reset')
p2.xaxis.visible = True
p2.yaxis.visible = True
​
p2.min_border_left = 1
p2.min_border_right = 1
p2.min_border_top = 1
p2.min_border_bottom = 1
​
p2.x_range = bkM.Range1d(-0.06,0.06)
p2.y_range = bkM.Range1d(-0.03,0.03)
​
p2.toolbar_location = None
​
x = processedGBTrial.get_group(trialNum).rotatedGazePoint_fr_XYZ.values[0,0]#rotatedGazePoint_fr_XYZ[0,0]
y = processedGBTrial.get_group(trialNum).rotatedGazePoint_fr_XYZ.values[0,1]#rotatedGazePoint_fr_XYZ[0,1]
gazeLoc = p2.x([x], [y], name = 'gaze',size=10)
​
x = processedGBTrial.get_group(trialNum).rotatedBallOnScreen_fr_XYZ.values[0,0]
y = processedGBTrial.get_group(trialNum).rotatedBallOnScreen_fr_XYZ.values[0,0]
ballLoc = p2.circle([x], [y], name = 'ball',size=10)
​
timeString = 't=%1.2f' % gbTrial.get_group(trialNum)['frameTime'].values[0]
​
yLims = [p2.y_range.start,p2.y_range.end]
xLims = [0,p2.plot_width]
​
top = yLims[1]
left = 0
​
#tBox = p2.text(text = [timeString],x = -1.1, y = 1.1, text_align = 'left',text_baseline='top')
tBox = p2.text(text = [timeString],x = -1.1, y = 1.1) #, text_align = 'left',text_baseline='top')
​
​

def update( winDur = 2, midFrame = 0):
    
    midTime = gbTrial.get_group(trialNum)['frameTime'].values[midFrame]
    
    p1.x_range.start = midTime - (winDur *0.5)
    p1.x_range.end = midTime + (winDur *0.5)
​
    gazeLoc.data_source.data['x'] = [processedGBTrial.get_group(trialNum).rotatedGazePoint_fr_XYZ.values[midFrame,0]]
    gazeLoc.data_source.data['y'] = [processedGBTrial.get_group(trialNum).rotatedGazePoint_fr_XYZ.values[midFrame,1]]
    
    ballLoc.data_source.data['x'] = [processedGBTrial.get_group(trialNum).rotatedBallOnScreen_fr_XYZ.values[midFrame,0]]
    ballLoc.data_source.data['y'] = [processedGBTrial.get_group(trialNum).rotatedBallOnScreen_fr_XYZ.values[midFrame,1]]
    
    timeString = 't=%1.2f' % gbTrial.get_group(trialNum)['frameTime'].values[midFrame]
    tBox.data_source.data = {'text': timeString}
    
    push_notebook()
​

gazeFigs = bkP.hplot(p1,p2)
bkP.show(gazeFigs)

numRows = len(gbTrial.get_group(trialNum)['frameTime'].values)
tRes = 0.01
interact(update, midFrame=(0,numRows,1),winDur=(0.1,5,.1))

trialID = 25
trialNum = trialID
gazeVelocity = processedDataFrame['cycGazeVelocity_fr'].values
#filteredGazeVelocity = sig.medfilt(gazeVelocity, 5)
#gbTrials = s1Processed.groupby('trialNumber')
trialFrames  = gbTrial.get_group(trialNum).index
​
angleList = []
for i in range(len(processedDataFrame)):
    temp = pF.vectorAngle(processedDataFrame.rotatedGazePoint_fr_XYZ.values[i], processedDataFrame.rotatedBallOnScreen_fr_XYZ.values[i])
    angleList.append(temp)
In [10]:

eyeToScreenDistance = 0.0725
gazeError = np.array(processedDataFrame.rotatedGazePoint_fr_XYZ.values - processedDataFrame.rotatedBallOnScreen_fr_XYZ.values)
x = gazeError[:,0]
y = gazeError[:,1]
z = gazeError[:,2]
​
x = np.array(x,dtype = float)
y = np.array(y,dtype = float)
z = np.array(z,dtype = float)
​
tempDf = []
if ('gazeBallError_fr_XYZ','X') in processedDataFrame.columns:
​
    processedDataFrame['gazeBallError_fr_XYZ','X'] = (180/np.pi)*np.arctan(x/eyeToScreenDistance)
    processedDataFrame['gazeBallError_fr_XYZ','Y'] = (180/np.pi)*np.arctan(y/eyeToScreenDistance)
    processedDataFrame['gazeBallError_fr_XYZ','Z'] = (180/np.pi)*np.arctan(z/eyeToScreenDistance)
    processedDataFrame['gazeAngularError_fr_degree', ''] = np.array(angleList, dtype = float)
​
else:
​
    tempDf = pd.DataFrame({
            ('gazeBallError_fr_XYZ', 'X') : (180/np.pi)*np.arctan(x/eyeToScreenDistance),
            ('gazeBallError_fr_XYZ', 'Y') : (180/np.pi)*np.arctan(y/eyeToScreenDistance),
            ('gazeBallError_fr_XYZ', 'Z') : (180/np.pi)*np.arctan(z/eyeToScreenDistance),
            ('gazeAngularError_fr_degree', ''): np.array(angleList, dtype = float),
                        })
    processedDataFrame = pd.concat([processedDataFrame,tempDf],axis=1,verify_integrity=True)
​


p1 = pF.timeSeries(frametime_fr = processedDataFrame['frameTime'].values,
                   yDataList = [angleList],
                   events_fr = processedDataFrame['eventFlag'].values,
                   yLabel='gaze velocity',
                   yLims = [0,90],
                   legendLabels = ['Gaze Velocity', 'Angular Error'],
                   plotHeight=600)
​
p1.title = 'Angular gaze Error'
​
p1.plot_width = 1000
​
yLims = [p1.y_range.start,p1.y_range.end]
xLims = [0,p1.plot_width]
​
tBox = bkM.BoxAnnotation(
                         bottom = yLims[0],
                         top = yLims[1],
                         left = np.mean(xLims)-3, left_units='screen',
                         right = np.mean(xLims)+3, right_units='screen',
                         fill_alpha=0.5, fill_color='green', level='overlay')
​
p1.renderers.extend([tBox])
#bkP.plot(p1)
​
bkP.show(p1)

offsetFrame = 8
trialStartIdx = processedDataFrame[processedDataFrame['eventFlag'] == 'trialStart'].index.tolist()
ballOffIdx = processedDataFrame[processedDataFrame['eventFlag'] == 'ballRenderOff'].index.tolist()
ballOnIdx = processedDataFrame[processedDataFrame['eventFlag'] == 'ballRenderOn'].index.tolist()
ballOnPaddleIdx = processedDataFrame[processedDataFrame['eventFlag'] == 'ballOnPaddle'].index.tolist()
print 'Number of Successful Trials: ', len(ballOnPaddleIdx), 'out of', len(trialStartIdx)
​

gazeError = processedDataFrame['gazeBallError_fr_XYZ'].values
angleList = processedDataFrame['gazeAngularError_fr_degree'].values
​
distanceAtBallOn = np.zeros((1,3))
distanceAfterBallOn = np.zeros((1,3))
distanceAtBallOff = np.zeros((1,3))
distanceAfterBallOff = np.zeros((1,3))
distanceAtBallOnPaddle = np.zeros((1,3))
​
angleAtBallOn = []
angleAfterBallOn = []
angleAtBallOff = []
angleAfterBallOff = []
angleAtBallOnPaddle = []
​
for i in range(len(trialInfoDataFrame)):
    distanceAtBallOn = np.vstack((distanceAtBallOn, gazeError[ballOnIdx[i]]))
    distanceAfterBallOn = np.vstack((distanceAfterBallOn, gazeError[ballOnIdx[i] + offsetFrame]))
    distanceAtBallOff = np.vstack((distanceAtBallOff, gazeError[ballOffIdx[i]]))
    distanceAfterBallOff = np.vstack((distanceAfterBallOff, gazeError[ballOffIdx[i] + offsetFrame]))
​
    angleAtBallOn.append(angleList[ballOnIdx[i]])
    angleAfterBallOn.append(angleList[ballOnIdx[i] + offsetFrame])
    angleAtBallOff.append(angleList[ballOffIdx[i]])
    angleAfterBallOff.append(angleList[ballOffIdx[i] + offsetFrame])
​
distanceAtBallOn = np.delete(distanceAtBallOn, 0,0)
distanceAfterBallOn = np.delete(distanceAfterBallOn, 0,0)
distanceAtBallOff = np.delete(distanceAtBallOff, 0,0)
distanceAfterBallOff = np.delete(distanceAfterBallOff, 0,0)
​
velocities_X = rawDataFrame['ballVel','X'].values[trialInfoDataFrame['firstFrame']]
velocities_Y = rawDataFrame['ballVel','Y'].values[trialInfoDataFrame['firstFrame']]
velocities_Z = rawDataFrame['ballVel','Z'].values[trialInfoDataFrame['firstFrame']]
​
​
tempDf = []
if ('gazeErrorAtBallOn_tr_XYZ','X') in trialInfoDataFrame.columns:
​
    trialInfoDataFrame['gazeErrorAtBallOn_tr_XYZ','X'] = distanceAtBallOn[:,0]
    trialInfoDataFrame['gazeErrorAtBallOn_tr_XYZ','Y'] = distanceAtBallOn[:,1]
    trialInfoDataFrame['gazeErrorAtBallOn_tr_XYZ','Z'] = distanceAtBallOn[:,2]
​
    trialInfoDataFrame['gazeErrorAtBallOff_tr_XYZ','X'] = distanceAtBallOff[:,0]
    trialInfoDataFrame['gazeErrorAtBallOff_tr_XYZ','Y'] = distanceAtBallOff[:,1]
    trialInfoDataFrame['gazeErrorAtBallOff_tr_XYZ','Z'] = distanceAtBallOff[:,2]
​
    trialInfoDataFrame['gazeErrorAfterBallOn_tr_XYZ','X'] = distanceAfterBallOn[:,0]
    trialInfoDataFrame['gazeErrorAfterBallOn_tr_XYZ','Y'] = distanceAfterBallOn[:,1]
    trialInfoDataFrame['gazeErrorAfterBallOn_tr_XYZ','Z'] = distanceAfterBallOn[:,2]
​
    trialInfoDataFrame['gazeErrorAfetrBallOff_tr_XYZ','X'] = distanceAfterBallOff[:,0]
    trialInfoDataFrame['gazeErrorAfterBallOff_tr_XYZ','Y'] = distanceAfterBallOff[:,1]
    trialInfoDataFrame['gazeErrorAfterBallOff_tr_XYZ','Z'] = distanceAfterBallOff[:,2]
    
    trialInfoDataFrame['ballVelocity_tr_XYZ','X'] = np.around(np.array(velocities_X, dtype = float), decimals=1)
    trialInfoDataFrame['ballVelocity_tr_XYZ','Y'] = np.around(np.array(velocities_Y, dtype = float), decimals=1)
    trialInfoDataFrame['ballVelocity_tr_XYZ','Z'] = np.around(np.array(velocities_Z, dtype = float), decimals=1)
​
    trialInfoDataFrame['gazeErrorAtBallOn_tr_degree', ''] = angleAtBallOn
    trialInfoDataFrame['gazeErrorAfterBallOn_tr_degree', ''] = angleAfterBallOn
    trialInfoDataFrame['gazeErrorAtBallOff_tr_degree', ''] = angleAtBallOff
    trialInfoDataFrame['gazeErrorAfterBallOff_tr_degree', ''] = angleAfterBallOff
​
    
else:
​
    tempDf = pd.DataFrame({
            ('gazeErrorAtBallOn_tr_XYZ', 'X') : distanceAtBallOn[:,0],
            ('gazeErrorAtBallOn_tr_XYZ', 'Y') : distanceAtBallOn[:,1],
            ('gazeErrorAtBallOn_tr_XYZ', 'Z') : distanceAtBallOn[:,2],
​
            ('gazeErrorAtBallOff_tr_XYZ','X') : distanceAtBallOff[:,0],
            ('gazeErrorAtBallOff_tr_XYZ','Y') : distanceAtBallOff[:,1],
            ('gazeErrorAtBallOff_tr_XYZ','Z') : distanceAtBallOff[:,2],
​
            ('gazeErrorAfterBallOn_tr_XYZ','X') : distanceAfterBallOn[:,0],
            ('gazeErrorAfterBallOn_tr_XYZ','Y') : distanceAfterBallOn[:,1],
            ('gazeErrorAfterBallOn_tr_XYZ','Z') : distanceAfterBallOn[:,2],
​
            ('gazeErrorAfterBallOff_tr_XYZ','X') : distanceAfterBallOff[:,0],
            ('gazeErrorAfterBallOff_tr_XYZ','Y') : distanceAfterBallOff[:,1],
            ('gazeErrorAfterBallOff_tr_XYZ','Z') : distanceAfterBallOff[:,2],
​
            ('ballVelocity_tr_XYZ','X') : np.around(np.array(velocities_X, dtype = float), decimals=1),
            ('ballVelocity_tr_XYZ','Y') : np.around(np.array(velocities_Y, dtype = float), decimals=1),
            ('ballVelocity_tr_XYZ','Z') : np.around(np.array(velocities_Z, dtype = float), decimals=1),
​
            ('gazeErrorAtBallOn_tr_degree', '') : angleAtBallOn,
            ('gazeErrorAfterBallOn_tr_degree', '') : angleAfterBallOn,
            ('gazeErrorAtBallOff_tr_degree', '') : angleAtBallOff,
            ('gazeErrorAfterBallOff_tr_degree', '') : angleAfterBallOff,
                        })
    trialInfoDataFrame = pd.concat([trialInfoDataFrame,tempDf],axis=1,verify_integrity=True)
​
​
def errorbar(fig, x, y, xerr=None, yerr=None, color='orange', point_kwargs={}, error_kwargs={}):
​
    fig.circle(x, y, color=color, **point_kwargs)
​
​
    if xerr is not None:
      x_err_x = []
      x_err_y = []
      for px, py, err in zip(x, y, xerr):
          x_err_x.append((px - err, px + err))
          x_err_y.append((py, py))
      fig.multi_line(x_err_x, x_err_y, color=color, **error_kwargs)
​
    if yerr is not None:
      y_err_x = []
      y_err_y = []
      for px, py, err in zip(x, y, yerr):
          y_err_x.append((px, px))
          y_err_y.append((py - err, py + err))
      fig.multi_line(y_err_x, y_err_y, color=color, **error_kwargs)

def plotMainEffect(dataFrame1, xVar1, yVar, dataFrame2 = None, xVar2 = None, signed = True, color = 'orange', plotTitle = False):
​
    mean = []
    std = []
    # plot the points
    p = bkP.figure(width=800, height=400)
​
    if dataFrame2 is None:
        groupedBy =  dataFrame1.groupby(xVar)
        for key in groupedBy.groups.keys():
            if (signed is True):
                mean.append(np.mean(groupedBy.get_group(key)[yVar].values))
                std.append(np.std(groupedBy.get_group(key)[yVar].values))
            else: 
                mean.append(np.mean(abs(groupedBy.get_group(key)[yVar].values)))
                std.append(np.std(abs(groupedBy.get_group(key)[yVar].values)))
        xs = np.unique(dataFrame1[xVar1].round(decimals =2))
        p.xaxis.axis_label = xVar1
​
    else:
        groupedBy =  dataFrame2.groupby(xVar2)
        for value, index in groupedBy.groups.items():
            if (signed is True):
                mean.append(np.mean(dataFrame1[yVar].values[index]))
                std.append(np.std(dataFrame1[yVar].values[index]))
            else: 
                mean.append(np.mean(abs(dataFrame1[yVar].values[index])))
                std.append(np.std(abs(dataFrame1[yVar].values[index])))
        xs = np.unique(dataFrame1[xVar1].round(decimals =2))
        p.xaxis.axis_label = xVar2 + '_'+ xVar1[1]
​
    yerr = std
    ys = mean
​
    p.line(xs, ys, color=color,line_width=2)
    errorbar(p,xs,ys, yerr=yerr, color = color,point_kwargs={'size': 10}, error_kwargs={'line_width': 3})
​
    p.yaxis.axis_label = ''.join(yVar)
​
    if( plotTitle ):
        p.title = plotTitle
        
    return p

xVar = 'preBlankDur'
yVar = 'gazeErrorAtBallOn_tr_XYZ','Y'
p1 = plotMainEffect(trialInfoDataFrame,xVar,yVar, None, None, False, 'red','Gaze Error (Y) at Ball On')
#(trialInfoDataFrame, xVar, yVar, tempDf, myKey, False, 'red', 'Gaze Error (Y) at Ball Off')
p1.y_range = bkM.Range1d(0,10)
​
​
xVar = 'preBlankDur'
yVar = 'gazeErrorAtBallOn_tr_XYZ','X'
p2 = plotMainEffect(trialInfoDataFrame,xVar,yVar, None, None, False, 'blue','Gaze Error (X) at Ball On')
p2.y_range = bkM.Range1d(0,10)
​
p = vplot(p1,p2)
bkP.show(p)

xVar = 'preBlankDur'
yVar = 'gazeErrorAfterBallOn_tr_XYZ','Y'
p1 = plotMainEffect(trialInfoDataFrame,xVar,yVar, None, None, False, 'red','Gaze Error (Y) after Ball On')
​
p1.y_range = bkM.Range1d(0,10)
​
​
xVar = 'preBlankDur'
yVar = 'gazeErrorAfterBallOn_tr_XYZ','X'
p2 = plotMainEffect(trialInfoDataFrame,xVar,yVar, None, None, False, 'blue','Gaze Error (X) after Ball On')
p2.y_range = bkM.Range1d(0,10)
​
p = vplot(p1,p2)
bkP.show(p)

xVar = 'preBlankDur'
yVar = 'gazeErrorAtBallOff_tr_XYZ','Y'
p1 = plotMainEffect(trialInfoDataFrame,xVar,yVar, None, None, False, 'red','Gaze Error (Y) at Ball Off')
p1.y_range = bkM.Range1d(0,10)
​
​
xVar = 'preBlankDur'
yVar = 'gazeErrorAtBallOff_tr_XYZ','X'
p2 = plotMainEffect(trialInfoDataFrame,xVar,yVar, None, None, False, 'blue','Gaze Error (X) at Ball Off')
p2.y_range = bkM.Range1d(0,10)
​
p = vplot(p1,p2)
bkP.show(p)

xVar = 'preBlankDur'
yVar = 'gazeErrorAfterBallOff_tr_XYZ','Y'
p1 = plotMainEffect(trialInfoDataFrame,xVar,yVar, None, None, False, 'red','Gaze Error (Y) after Ball Off')
p1.y_range = bkM.Range1d(0,10)
​
​
xVar = 'preBlankDur'
yVar = 'gazeErrorAfterBallOff_tr_XYZ','X'
p2 = plotMainEffect(trialInfoDataFrame,xVar,yVar, None, None, False, 'blue','Gaze Error (X) after Ball Off')
p2.y_range = bkM.Range1d(0,10)
​
p = vplot(p1,p2)
bkP.show(p)

xVar = 'postBlankDur'
yVar = 'gazeErrorAtBallOn_tr_XYZ','Y'
p1 = plotMainEffect(trialInfoDataFrame,xVar,yVar, None, None, False, 'red','Gaze Error (Y) at Ball On')
p1.y_range = bkM.Range1d(0,10)
​
​
xVar = 'postBlankDur'
yVar = 'gazeErrorAtBallOn_tr_XYZ','X'
p2 = plotMainEffect(trialInfoDataFrame,xVar,yVar, None, None, False, 'blue','Gaze Error (X) at Ball On')
p2.y_range = bkM.Range1d(0,10)
​
p = vplot(p1,p2)
bkP.show(p)

xVar = 'postBlankDur'
yVar = 'gazeErrorAtBallOff_tr_XYZ','Y'
p1 = plotMainEffect(trialInfoDataFrame,xVar,yVar, None, None, False, 'red','Gaze Error (Y) at Ball Off')
p1.y_range = bkM.Range1d(0,10)
​
​
xVar = 'postBlankDur'
yVar = 'gazeErrorAtBallOff_tr_XYZ','X'
p2 = plotMainEffect(trialInfoDataFrame,xVar,yVar, None, None, False, 'blue','Gaze Error (X) at Ball Off')
p2.y_range = bkM.Range1d(0,10)
​
p = vplot(p1,p2)
bkP.show(p)

xVar = 'ballVelocity_tr_XYZ', 'X'
myKey = 'ballVelocity'
yVar = 'gazeErrorAtBallOn_tr_XYZ','Y'
​
mean = []
std = []
tempDf = []
tempDf = pd.DataFrame({
        ('ballVelocity') : trialInfoDataFrame[xVar].values,
                    })
p1 = plotMainEffect(trialInfoDataFrame, xVar, yVar, tempDf, myKey, False, 'red', 'Gaze Error (Y) at Ball On')
p1.y_range = bkM.Range1d(0,10)
​
yVar = 'gazeErrorAtBallOn_tr_XYZ','X'
p2 = plotMainEffect(trialInfoDataFrame, xVar, yVar, tempDf, myKey, False, 'blue', 'Gaze Error (X) at Ball On')
p2.y_range = bkM.Range1d(0,10)
​
​
p = vplot(p1,p2)
bkP.show(p)
​

xVar = 'ballVelocity_tr_XYZ', 'X'
myKey = 'ballVelocity'
yVar = 'gazeErrorAtBallOff_tr_XYZ','Y'
​
mean = []
std = []
tempDf = []
tempDf = pd.DataFrame({
        ('ballVelocity') : trialInfoDataFrame[xVar].values,
                    })
p1 = plotMainEffect(trialInfoDataFrame, xVar, yVar, tempDf, myKey, False, 'red', 'Gaze Error (Y) at Ball Off')
p1.y_range = bkM.Range1d(0,10)
​
yVar = 'gazeErrorAtBallOff_tr_XYZ','X'
p2 = plotMainEffect(trialInfoDataFrame, xVar, yVar, tempDf, myKey, False, 'blue', 'Gaze Error (X) at Ball Off')
p2.y_range = bkM.Range1d(0,10)
​
​
p = vplot(p1,p2)
bkP.show(p)
​

xVar = 'ballVelocity_tr_XYZ', 'Z'
myKey = 'ballVelocity'
yVar = 'gazeErrorAtBallOn_tr_XYZ','Y'
​
mean = []
std = []
tempDf = []
tempDf = pd.DataFrame({
        ('ballVelocity') : trialInfoDataFrame[xVar].values,
                    })
p1 = plotMainEffect(trialInfoDataFrame, xVar, yVar, tempDf, myKey, False, 'red', 'Gaze Error (Y) at Ball On')
p1.y_range = bkM.Range1d(0,10)
​
yVar = 'gazeErrorAtBallOn_tr_XYZ','X'
p2 = plotMainEffect(trialInfoDataFrame, xVar, yVar, tempDf, myKey, False, 'blue', 'Gaze Error (X) at Ball On')
p2.y_range = bkM.Range1d(0,10)
​
​
p = vplot(p1,p2)
bkP.show(p)
​

xVar = 'ballVelocity_tr_XYZ', 'Z'
myKey = 'ballVelocity'
yVar = 'gazeErrorAtBallOff_tr_XYZ','Y'
​
mean = []
std = []
tempDf = []
tempDf = pd.DataFrame({
        ('ballVelocity') : trialInfoDataFrame[xVar].values,
                    })
p1 = plotMainEffect(trialInfoDataFrame, xVar, yVar, tempDf, myKey, False, 'red', 'Gaze Error (Y) at Ball Off')
p1.y_range = bkM.Range1d(0,10)
​
yVar = 'gazeErrorAtBallOff_tr_XYZ','X'
p2 = plotMainEffect(trialInfoDataFrame, xVar, yVar, tempDf, myKey, False, 'blue', 'Gaze Error (X) at Ball Off')
p2.y_range = bkM.Range1d(0,10)
​
​
p = vplot(p1,p2)
bkP.show(p)
​

p1 = bkP.figure(title="Error at Ball On",tools="xpan,ypan,reset,save,xwheel_zoom,ywheel_zoom,resize,tap,hover"
                ,x_range=(-40, 40), y_range=(-40, 40), plot_width=500, plot_height=500)
x = distanceAtBallOn[:,0]
y = distanceAtBallOn[:,1]
x = np.array(x,dtype = float)
y = np.array(y,dtype = float)
p1.scatter(x, y, marker="circle", size=10,
              line_color="navy", fill_color="orange", alpha=0.5)
p1.xaxis.axis_label = "degree of Visual Angle"
#p1.xaxis.axis_line_width = 3
​
p2 = bkP.figure(title="Error at Ball Off",tools="xpan,ypan,reset,save,xwheel_zoom,ywheel_zoom,resize,tap,hover"
                ,x_range=(-40, 40), y_range=(-40, 40), plot_width=500, plot_height=500)
​
x = distanceAtBallOff[:,0]
y = distanceAtBallOff[:,1]
x = np.array(x,dtype = float)
y = np.array(y,dtype = float)
p2.scatter(x, y, marker="circle", size=10,
              line_color="navy", fill_color="orange", alpha=0.5)
p2.xaxis.axis_label = "degree of Visual Angle"
#p2.xaxis.axis_line_width = 3
​
​
p = hplot(p2,p1)
bkP.show(p)

p1 = bkP.figure(title="Error Correlation",tools="xpan,ypan,reset,save,xwheel_zoom,ywheel_zoom,resize,tap,hover"
                ,x_range=(-40, 40), y_range=(-40, 40), plot_width=500, plot_height=500)
x = processedDataFrame['gazeAngularError_fr_degree'].values[ballOffIdx]
y = processedDataFrame['gazeAngularError_fr_degree'].values[ballOnIdx]
x = np.array(x,dtype = float)
y = np.array(y,dtype = float)
p1.scatter(x,y, marker="circle", size=10,
              line_color="navy", fill_color="purple", alpha=0.5)
p1.xaxis.axis_label = "Error at Ball Off"
p1.yaxis.axis_label = "Error at Ball On"
​
#p1.xaxis.axis_line_width = 3
​
p2 = bkP.figure(title="Error Correlation",tools="xpan,ypan,reset,save,xwheel_zoom,ywheel_zoom,resize,tap,hover"
                ,x_range=(-40, 40), y_range=(-40, 40), plot_width=500, plot_height=500)
​
x = processedDataFrame['gazeAngularError_fr_degree'].values[ballOnPaddleIdx]
y = processedDataFrame['gazeAngularError_fr_degree'].values[ballOnIdx]
​
x = np.array(x,dtype = float)
y = np.array(y,dtype = float)
p2.scatter(x, y, marker="circle", size=10,
              line_color="navy", fill_color="purple", alpha=0.5)
p2.xaxis.axis_label = "Error at Ball On Paddle"
p2.yaxis.axis_label = "Error at Ball On"
#p2.xaxis.axis_line_width = 3
​
​
p = hplot(p1,p2)
bkP.show(p)
​
xCatch = []
yCatch = []
xMiss = []
yMiss = []
for i in range(max(processedDataFrame['trialNumber'].values)):
    if ( trialInfoDataFrame['ballCaughtQ'].values[i] == True ):
        xCatch.append(distanceAtBallOn[i,0])
        yCatch.append(distanceAtBallOn[i,1])
    else:
        xMiss.append(distanceAtBallOn[i,0])
        yMiss.append(distanceAtBallOn[i,1])
        
    
p1 = bkP.figure(title="Error at Ball On",tools="xpan,ypan,reset,save,xwheel_zoom,ywheel_zoom,resize,tap,hover"
                ,x_range=(-40, 40), y_range=(-40, 40), plot_width=500, plot_height=500)
#x = distanceAtBallOn[:,0]
#y = distanceAtBallOn[:,1]
print len(xCatch)
print len(xMiss)
x = xCatch
y = yCatch
x = np.array(x,dtype = float)
y = np.array(y,dtype = float)
p1.scatter(x, y, marker="circle", size=10,
              line_color="navy", fill_color="orange", alpha=0.5)
x = xMiss
y = yMiss
x = np.array(x,dtype = float)
y = np.array(y,dtype = float)
p1.scatter(x, y, marker="circle", size=10,
              line_color="red", fill_color="purple", alpha=0.5)
​
p1.xaxis.axis_label = "degree of Visual Angle"
#p1.xaxis.axis_line_width = 3
​
​
​
p2 = bkP.figure(title="Error after Ball On ("+ str(offsetFrame)+' Frames)',tools="xpan,ypan,reset,save,xwheel_zoom,ywheel_zoom,resize,tap,hover"
                ,x_range=(-40, 40), y_range=(-40, 40), plot_width=500, plot_height=500)
​
xCatch = []
yCatch = []
xMiss = []
yMiss = []
for i in range(max(processedDataFrame['trialNumber'].values)):
    if ( trialInfoDataFrame['ballCaughtQ'].values[i] == True ):
        xCatch.append(distanceAfterBallOn[i,0])
        yCatch.append(distanceAfterBallOn[i,1])
    else:
        xMiss.append(distanceAfterBallOn[i,0])
        yMiss.append(distanceAfterBallOn[i,1])
​
x = xCatch
y = yCatch
​
#x = distanceAfterBallOn[:,0]
#y = distanceAfterBallOn[:,1]
​
x = np.array(x,dtype = float)
y = np.array(y,dtype = float)
p2.scatter(x, y, marker="circle", size=10,
              line_color="navy", fill_color="orange", alpha=0.5)
x = xMiss
y = yMiss
x = np.array(x,dtype = float)
y = np.array(y,dtype = float)
p2.scatter(x, y, marker="circle", size=10,
              line_color="red", fill_color="purple", alpha=0.5)
​
        
p2.xaxis.axis_label = "degree of Visual Angle"
#p2.xaxis.axis_line_width = 3
​
​
p = hplot(p1,p2)
bkP.show(p)
​

p1 = bkP.figure(title="Error at Ball Off",tools="xpan,ypan,reset,save,xwheel_zoom,ywheel_zoom,resize,tap,hover"
                ,x_range=(-40, 40), y_range=(-40, 40), plot_width=500, plot_height=500)
x = distanceAtBallOff[:,0]
y = distanceAtBallOff[:,1]
x = np.array(x,dtype = float)
y = np.array(y,dtype = float)
p1.scatter(x, y, marker="circle", size=10,
              line_color="navy", fill_color="orange", alpha=0.5)
p1.xaxis.axis_label = "degree of Visual Angle"
#p1.xaxis.axis_line_width = 3
​
p2 = bkP.figure(title="Error after Ball Off("+ str(offsetFrame)+' Frames)',tools="xpan,ypan,reset,save,xwheel_zoom,ywheel_zoom,resize,tap,hover"
                ,x_range=(-40, 40), y_range=(-40, 40), plot_width=500, plot_height=500)
​
x = distanceAfterBallOff[:,0]
y = distanceAfterBallOff[:,1]
x = np.array(x,dtype = float)
y = np.array(y,dtype = float)
p2.scatter(x, y, marker="circle", size=10,
              line_color="navy", fill_color="orange", alpha=0.5)
p2.xaxis.axis_label = "degree of Visual Angle"
#p2.xaxis.axis_line_width = 3
​
​
p = hplot(p1,p2)
bkP.show(p)
​

sessionDict['processed'] = processedDataFrame
sessionDict['trialInfo'] = trialInfoDataFrame
#sessionDict['statistics'] = tempDict
pd.to_pickle(sessionDict, filePath + fileName + '.pickle')

​
predictionIdx = []
for trialID in range(len(trialInfoDataFrame)):
​
    ballPosition_fr_XYZ = np.array(rawDataFrame['ballPos'].values)
    ballVelocity_fr_XYZ = np.array(rawDataFrame['ballVel'].values)
    ballPosition_fr_XYZ.shape
    p1  = ballPosition_fr_XYZ[ballOnIdx[trialID], :]
    predictionTime = abs(p1[2]/ballVelocity_fr_XYZ[ballOnIdx[trialID], 2]) + rawDataFrame['frameTime'].values[ballOnIdx[trialID]]
    timeWindow = np.array(rawDataFrame['frameTime'].values[ballOnIdx[trialID] : ballOnIdx[trialID] + 50])
    tempArray = abs(timeWindow - predictionTime)
    predictionIndex = ballOnIdx[trialID] + np.where(tempArray == min(tempArray))
​
    #print 'ballOnFrame =', rawDataFrame['frameTime'].values[ballOnIdx[trialID]]
    #print 'prediction Time =',predictionTime
    #print 'index = ', predictionIndex
    #print 'found predicted time =', rawDataFrame['frameTime'].values[predictionIndex]
    predictionIdx.append(predictionIndex[0,0]) # Because ther is a strange way that np.where works
#print predictionIdx
tempDf = []
if ('predictiveFrame','') in trialInfoDataFrame.columns:
​
    trialInfoDataFrame['predictiveFrame', ''] = predictionIdx
    trialInfoDataFrame['ballOnFrame', ''] = ballOnIdx
    trialInfoDataFrame['ballOffFrame', ''] = ballOffIdx
    
else:
​
    tempDf = pd.DataFrame({
            ('predictiveFrame', '') : predictionIdx,
            ('ballOnFrame', '') : ballOnIdx,
            ('ballOffFrame', '') : ballOffIdx,
                        })
    trialInfoDataFrame = pd.concat([trialInfoDataFrame,tempDf],axis=1,verify_integrity=True)
​
​
p1 = rawDataFrame['ballPos'].values[trialInfoDataFrame['predictiveFrame'].values]
p2 = rawDataFrame['paddlePos'].values[trialInfoDataFrame['ballOnFrame'].values]
predictiveCatchError = p2 - p1

color = []
missFail = trialInfoDataFrame['ballCaughtQ'].values
for i in missFail:
    if i:
        color.append('green')
    else:
        color.append('red')
xMax = 2
yMax = 2
p1 = bkP.figure(title="Predictive Catch Error",tools="xpan,ypan,reset,save,xwheel_zoom,ywheel_zoom,resize,tap,hover"
                ,x_range=(-xMax, xMax), y_range=(-yMax, yMax), plot_width=500, plot_height=500)
x = predictiveCatchError[:,0]
y = predictiveCatchError[:,1]
x = np.array(x,dtype = float)
y = np.array(y,dtype = float)
p1.scatter(x, y, marker="circle", size=10,
              line_color="navy", fill_color=color, alpha=0.5)
p1.xaxis.axis_label = "X [m]"
p1.yaxis.axis_label = "Y [m]"
​
p2 = bkP.figure(title="Predictive Catch Error",tools="xpan,ypan,reset,save,xwheel_zoom,ywheel_zoom,resize,tap,hover"
                ,x_range=(-xMax, xMax), y_range=(-yMax, yMax), plot_width=500, plot_height=500)
​
x = predictiveCatchError[:,2]
y = predictiveCatchError[:,1]
x = np.array(x,dtype = float)
y = np.array(y,dtype = float)
p2.scatter(x, y, marker="circle", size=10,
              line_color="navy", fill_color=color, alpha=0.5)
p2.xaxis.axis_label = "Z [m]"
p2.yaxis.axis_label = "Y [m]"
​
​
p = hplot(p1,p2)
bkP.show(p)
​

