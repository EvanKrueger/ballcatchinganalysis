"""
This module currently holds all the unused plotting code that I've removed from 
the performFun.py module. This can be expanded at a later date.
"""

def plotMainEffect(dF,xVar,yVar, plotTitle = False):
    groupedBy =  dF.groupby(xVar)
    pre_mean = []
    pre_std = []
    for gNum, gr in groupedBy:
        pre_mean.append(np.mean(gr[yVar].values))
        pre_std.append(np.std(gr[yVar].values))
    xs = np.unique(dF[xVar].round(decimals =2))
    yerr = pre_std
    ys = pre_mean
    p = bkP.figure(width=800, height=400)
    p.line(xs, ys, color='orange',line_width=2)
    errorbar(p,xs,ys, yerr=yerr,point_kwargs={'size': 10}, error_kwargs={'line_width': 3})
    p.xaxis.axis_label = xVar
    p.yaxis.axis_label = ''.join(yVar)
    if( plotTitle ):
        p.title = plotTitle
    return p
    
def errorbar(fig, x, y, xerr=None, yerr=None, color='red', point_kwargs={}, error_kwargs={}):
    fig.circle(x, y, color=color, **point_kwargs)
    if xerr is not None:
      x_err_x = []
      x_err_y = []
      for px, py, err in zip(x, y, xerr):
          x_err_x.append((px - err, px + err))
          x_err_y.append((py, py))
      fig.multi_line(x_err_x, x_err_y, color=color, **error_kwargs)
    if yerr is not None:
      y_err_x = []
      y_err_y = []
      for px, py, err in zip(x, y, yerr):
          y_err_x.append((px, px))
          y_err_y.append((py - err, py + err))
      fig.multi_line(y_err_x, y_err_y, color=color, **error_kwargs)
      
def plotInteraction(trialInfoDF,xVar,yVar, lineVar):
    lineNames = np.array(np.unique(trialInfoDF[lineVar].round(decimals =2)),dtype=np.str)
    groupedByPre =  trialInfoDF.groupby([lineVar,xVar])
    mean_cond = []
    std_cond  = []
    for gNum, gr in groupedByPre:
        mean_cond.append(np.mean(gr[yVar].values))
        std_cond.append(np.std(gr[yVar].values))
    mean_pre_post = np.reshape(mean_cond,[3,3])
    std_pre_post = np.reshape(std_cond,[3,3])
    xs = [np.unique(trialInfoDF[xVar].round(decimals =2))]*3
    ys = [np.array(xyz,dtype=np.float) for xyz in mean_pre_post]
    yerr = [np.array(xyz,dtype=np.float) for xyz in std_pre_post]
    p = bkP.figure(width=800, height=400)
    off = [-.01, 0, .01]
    clist = ['blue','orange','green']
    for j in range(3):
        p.line( xs[j]+off[j], ys[j] ,line_width=3,color=clist[j],legend = lineVar + ' ' +  lineNames[j])
        errorbar(p,xs[j]+off[j],ys[j], yerr=yerr[j],color=clist[j], point_kwargs={'line_width':3,'size': 10}, 
                 error_kwargs={'line_width': 3})
    p.xaxis.axis_label = xVar
    p.yaxis.axis_label = ''.join(yVar)
    return p

def timeSeries( frametime_fr=None, yDataList=None,yLabel=None,legendLabels=None,yLims = [0,300], events_fr=None,trialsStarts_tr=None,plotHeight=500,plotWidth = 1000):
    ''' 
    Creates a time-series plot of gaze data with Bokeh.
    dataFrame = a dataframe with field ['frameTime'], ['eventFlag'], and ['trialNumber'] 
    yLabel = A label for the Y axis. 
    yDataList = A list of vectors to be plotted on the Y axis as a line
    legendLabels = A list of names for data plotted on the Y axis
    yMax = Height of Y axidafdataFrames
    markEvents= Show vertical lines with labels at events in dataFrame['eventFlag']
    markTrials=Show vertical lines with labels at start of each trial
    '''
    from bokeh.palettes import Spectral6
    
    if( isinstance(yDataList, list) is False):
        raise TypeError('yDataList should be a list of lists.  Try [yData].')
    
    if( legendLabels and isinstance(legendLabels, list) is False):
        raise TypeError('legendLabels should be a list of lists.  Try [yLabelList].')
        
    #### Setup figure

    yRange = bkM.Range1d(yLims[0],yLims[1])

    p = bkP.figure(plot_width =plotWidth, plot_height=plotHeight,tools="xpan,reset,save,xwheel_zoom,resize,tap",
                   y_range=[0,500], 
                   x_range=[np.min(frametime_fr),np.max(frametime_fr)],
                   x_axis_label='time (s)', y_axis_label=yLabel)

    p.ygrid.grid_line_dash = [6, 4]

    #p.x_range = bkM.Range1d(dataFrame['frameTime'].values[0], dataFrame['frameTime'].values[0]+2)
    p.x_range = bkM.Range1d(np.min(frametime_fr), np.min(frametime_fr)+2)
    p.y_range = yRange

    ### Vertical lines at trial starts
    if( trialsStarts_tr ):
        
        X = [[startIdx]*2 for startIdx in trialsStarts_tr] #dataFrame.groupby('trialNumber')]
        Y = [[yLims[0],yLims[1]]] * len(trialsStarts_tr)
        p.multi_line(X,Y,color='red',alpha=0.6,line_width=2)

    ######################################################################################################
    ## Plot gaze velocity(s)
            
    for yIdx, yData in enumerate(yDataList):
        
        if(legendLabels and len(legendLabels) >= yIdx):
            p.line(frametime_fr,yData,line_width=3, alpha=.7,color=Spectral6[yIdx],legend=legendLabels[yIdx]) 
        else:
            p.line(frametime_fr,yData,line_width=3, alpha=.7,color=Spectral6[yIdx]) 

        #p.line(dataFrame['frameTime'].values,dataFrame['cycGIWVelocityRAW'].values,line_width=3,alpha=.7,color='green',legend="raw")
            

    ######################################################################################################
    ### Annotate events
    
    showHighBox = False
    
    #if( type(events_fr) is pd.Series ):
    if( events_fr.any() ):
        
        showHighBox = True
        X = frametime_fr[np.where(events_fr>2)]+.01
        Y = [yLims[1]*.9]*len(X)
        text = [str(event) for event in events_fr[np.where(events_fr>2)]]

        p.text(X,Y,text,text_font_size='8pt',text_font='futura')
        
        ### Vertical lines at events
        X = [ [X,X] for X in frametime_fr[np.where(events_fr>2)]]
        p.multi_line(X,[[yLims[0],yLims[1]*.9]] * len(X),color='red',alpha=0.6,line_width=2)

    if( trialsStarts_tr):
        
        showHighBox = True
        
        ### Annotate trial markers
        X = [trialStart+0.02 for trialStart in trialsStarts_tr]
        Y = [yLims[1]*.95] * len(trialsStarts_tr)
        text = [  'Tr ' + str(trIdx) for trIdx,trialStart in enumerate(trialsStarts_tr)]
        p.text(X,Y,text,text_font_size='10pt',text_font='futura',text_color='red')

        ### Vertical lines at trial starts
        X = [  [trialStart]*2 for trialStart in trialsStarts_tr]
        Y = [[yLims[0],yLims[1]*.9]] * len(trialsStarts_tr)
        p.multi_line(X,Y,color='red',alpha=0.6,line_width=4)

    if( showHighBox ):
        
        high_box = bkM.BoxAnnotation(plot=p, bottom = yLims[1]*.9, 
                                     top=yLims[1], fill_alpha=0.7, fill_color='green', level='underlay')
        p.renderers.extend([high_box])
        
    return p


def createLine( point0, point1 ):
        
    unitVector = np.subtract(point0, point1)/length(np.subtract(point0, point1))
    #print 'unitVector', unitVector
    return unitVector

def plotFilteredGazeData(rawDataFrame, processedDataFrame):
    from bokeh.io import vplot
    from bokeh.plotting import figure, output_file, show

    frameRange = range(20000,21000)
    x0 = rawDataFrame['cycEyeOnScreen']['X'][frameRange].values
    x1 = processedDataFrame['medFilt3_cycEyeOnScreen']['X'][frameRange].values
    x2 = processedDataFrame['avgFilt3_cycEyeOnScreen']['X'][frameRange].values
    x3 = processedDataFrame['avgFilt5_cycEyeOnScreen']['X'][frameRange].values

    y0 = rawDataFrame['cycEyeOnScreen']['Y'][frameRange].values
    y1 = processedDataFrame['medFilt3_cycEyeOnScreen']['Y'][frameRange].values
    y2 = processedDataFrame['avgFilt3_cycEyeOnScreen']['Y'][frameRange].values
    y3 = processedDataFrame['avgFilt5_cycEyeOnScreen']['Y'][frameRange].values

    T = range(len(x1))

    dataColor = ['red','green', 'blue', 'yellow']
    dataLegend = ["Raw", "Med3", "Med5", "Med7"]
    p1 = figure(plot_width=500, plot_height=500)
    p1.multi_line(xs=[T,T,T,T], ys=[x0, x1, x2, x3],
                 color=dataColor)

    p2 = figure(plot_width=500, plot_height=500)
    p2.multi_line(xs=[T,T,T,T], ys=[y0, y1, y2, y3],
                 color=dataColor)

    p = vplot(p1,p2)
    show(p)


def plotMyData_Scatter3D(data, label, color, marker, axisLabels):
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.scatter(data[:,0], data[:,2], data[:,1], label = label, c = color, marker = marker)

    ax.set_xlabel(axisLabels[0])
    ax.set_ylabel(axisLabels[1])
    ax.set_zlabel(axisLabels[2])

    legend = plt.legend(loc=[1.,0.4], shadow=True, fontsize='small')# 'upper center'
    plt.show()

def plotMyData_2D(data, title, label, color, marker, axisLabels, dataRange = None):
    """
    """ 
    fig1 = plt.figure()
    plt.plot(data[:,0], data[:,1], label = label, c = color, marker = marker)

    #plt.xlim(xmin, xmax)
    #plt.ylim(ymin, ymax)
    plt.xlabel(axisLabels[0])
    plt.ylabel(axisLabels[1])
    plt.title(title)
    plt.grid(True)
    #plt.axis('equal')
    legend = plt.legend(loc=[1.,0.4], shadow=True, fontsize='small')# 'upper center'
    plt.show()
