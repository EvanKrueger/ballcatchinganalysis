def pixelsToMetric(x_pixel, y_pixel, screen_size, resolution):
    """
    Return the location of where gaze vector intersects HMD screen in units of 
    meters.

    Parameters
    ----------
    x_pixel : array_like
        An N-dimensional array containing samples gaze location on screen in 
        units of pixels.
    y_pixel : array_like
        An N-dimensional array containing samples gaze location on screen in 
        units of pixels.
    screen_size : tuple
        Two-element tuple specifying the screen size in units of meters.
    resolution : tuple
        Two-element tuple specifying the screen resolution in units of pixels.

    Returns
    -------
    x_metric, y_metric : list of ndarray
    
    Notes
    -------
    0.126, 0.071 = Screen size (in meters) according to SMI manual
    """
    
    screen_x, screen_y = screen_size
    res_x, res_y = resolution
    
    ratio_x = screen_x / res_x
    ratio_y = screen_y / res_y
    
    # mult ratios by dist from screen center
    x_metric = ratio_x * (x_pixel - res_x / 2)
    y_metric = ratio_y * (y_pixel - res_y / 2)
    
    return x_metric, y_metric
    

def metricToPixels(x_metric, y_metric, screen_size, resolution):
    """
    Return the location of where gaze vector intersects HMD screen in units of 
    pixels.

    Parameters
    ----------
    x_metric : array_like
        An N-dimensional array containing samples gaze location on screen in 
        units of meters.
    y_metric : array_like
        An N-dimensional array containing samples gaze location on screen in 
        units of meters.
    screen_size : tuple
        Two-element tuple specifying the screen size in units of meters.
    resolution : tuple
        Two-element tuple specifying the screen resolution in units of pixels.

    Returns
    -------
    x_metric, y_metric : list of ndarray
    
    Notes
    -------
    0.126, 0.071 = Screen size (in meters) according to SMI manual
    """
    
    screen_x, screen_y = screen_size
    res_x, res_y = resolution
    
    ratio_x = res_x / screen_x
    ratio_y = res_y / screen_y
    
    # mult ratios by dist from screen center
    x_pixel = ratio_x * (x_metric + screen_x / 2)
    y_pixel = ratio_y * (y_metric + screen_y / 2)
    
    return x_pixel, y_pixel


def vectorAngle(v1, v2, unit="degrees"):
    """
    Return the angle between two vectors in radians or degrees.

    Parameters
    ----------
    v1 : array_like
        An 1-dimensional array
    v2 : array_like
        An 1-dimensional array
    
    Returns
    -------
    angle : float
        A floating point number representing the angle between input vectors.
    """
    # vector magnitudes
    len1 = np.linalg.norm(v1, ord=1)
    len2 = np.linalg.norm(v2, ord=1)
    
    # vector dot product
    dprod = np.dot(v1, v2)
    
    # angle between vectors
    angle = np.acos(dprod) / (len1 * len2)
    
    if unit == "degrees":
        angle = np.degrees(angle)
    
    return angle


def angularVelocity(cycPOR, viewQuat, dist_eye_screen, coordinateSystem='world'):
    """
    Return the location of where gaze vector intersects HMD screen in units of 
    meters.

    Parameters
    ----------
    cycPOR : array_like
        An N-dimensional array containing samples gaze location on screen in 
        units of pixels.
    y_pixel : array_like
        An N-dimensional array containing samples gaze location on screen in 
        units of pixels.
    screen_size : tuple
        Two-element tuple specifying the screen size in units of meters.
    resolution : tuple
        Two-element tuple specifying the screen resolution in units of pixels.
        
        
    cycPOR is point on 
    POR = the value reported by smi in pixel or metric units. Gaze point on display
        - this is given with respeoct to cyc eye point (origin) 
        - eye in head 
    viewQuat = heading direction (head pose, orientation only - in WCS)
    
    coord flag = false: eye movement only, WCS: both head and eye movement
    
    dist_eye_screen=0.0725
    """
    
    cycPOR_X = cycPOR[:, 0]
    cycPOR_Y = cycPOR[:, 1]
    cycPOR_Z = np.zeros(len(cycPOR) + dist_eye_screen)

    # Transform quaternions (why?)
    # TO ACCOUNT FOR HEAD POSITION
    viewRotMat_fr_mRow_mCol = [qu.Quat(np.array(q,dtype=np.float))._quat2transform() for q in tempDataFrame.viewQuat.values]

    # Now these PORs are some 3D points on the Screen still in HCS
    metricCycPOR_fr_XYZ = np.array([metricCycPOR_X, metricCycPOR_Y, metricCycPOR_Z], dtype = float).T


    if ( coordinateSystem == 'world' ):
        # If we want to calculate the Gaze vector in WCS we need to take into account Head Orientation
        # So we neead to multiply all 3D POR points with the Rotation Matrix calculated by Quaternions
        global_metricCycPOR_fr_XYZ = np.array([ np.dot(metricCycPOR_fr_XYZ[fr].T,viewRotMat_fr_mRow_mCol[fr]) 
             for fr in range(len(metricCycPOR_fr_XYZ))])
    else:
        # If not then the Gaze vector would be calculated in HCS 
        global_metricCycPOR_fr_XYZ = metricCycPOR_fr_XYZ


    # Now we want to calculate the derivative of position so we need to shift the array
    metricCycPOR_fr_XYZ_shifted = np.roll(global_metricCycPOR_fr_XYZ, -1, axis=0)

    timeArray = tempDataFrame.frameTime.values
    timeArray_shifted = np.roll(timeArray, -1)
    timeDiff = timeArray_shifted -  timeArray

    gazeAngle_fr = []
    temp = zip(global_metricCycPOR_fr_XYZ, metricCycPOR_fr_XYZ_shifted)
    for v1,v2 in temp:
        gazeAngle_fr.append(vectorAngle(v1, v2))
        
        
    gazeVelocity_fr = np.divide(gazeAngle_fr, timeDiff)
    
        
    return gazeVelocity_fr
