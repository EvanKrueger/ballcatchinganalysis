"""
This module is intended for use in demonstration of eye-tracking in VR.

Assumptions:
    - data will include 4x4 transformation matrix from local to world per frame
    - running Python3 with compatible Numpy and plotting packages   
"""
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits.mplot3d.art3d import Poly3DCollection
from itertools import product, combinations, permutations
import numpy as np
import time

class Rectangle():
    def __init__(self, x=1, y=1, z=1):
        """
        Define set of verticies upon instantiation.
        """
        self.xrange = [-x, x]
        self.yrange = [-y, y]
        self.zrange = [-z, z]

        # define coordinate values
        self.verticies = list(product(self.xrange, self.yrange, self.zrange))

    def rotate(self, xangle, yangle, zangle):
        """
        Updates shape coordinates through use of rotation matrix.
        Note: assumed to take as input rotations measured in degrees
        """
        # cast verticies as numpy matrix
        verts = np.asmatrix(self.verticies)

        # numpy functions assume radians
        xangle *= np.pi / 180
        yangle *= np.pi / 180
        zangle *= np.pi / 180

        # define rotation matrix
        xrotate = np.matrix([[1, 0, 1], [0, np.cos(xangle), -np.sin(xangle)], [0, np.sin(xangle), np.cos(xangle)]])
        yrotate = np.matrix([[np.cos(yangle), 0, np.sin(yangle)], [0, 1, 0], [-np.sin(yangle), 0, np.cos(yangle)]])
        zrotate = np.matrix([[np.cos(zangle), -np.sin(zangle), 0], [np.sin(zangle), np.cos(zangle), 0], [0, 0, 1]])

        # multiply verticies by transforms
        newVerts = xrotate * yrotate * zrotate * verts.T

        # update verticies
        self.verticies = newVerts.T.tolist()
        return

    
#    def disp(self


class AnimationPath(timeSamples, poseSamples):
    """
    Class for encapsulating the captured data, playback operations.
    """
    # need to read in time, matrix data per-sample from file
    self.times = timeSamples
    self.poses = poseSamples
    
    def playback(node, poses, fps=30):
        """
        function for updating motion of object when graphing (likely in plotly)
        """
        # time per frame in seconds
        delay = 1/fps
        
        # looping update to Node position
        # may or may not want some kind of refresh-rate enforcement
        for pose in poses:
            # record time duration of update to position 
            start = time.time()
            node.setPosition(pose)
            end = time.time()

            # sleep thread for constant-time update
            duration = end - start
            delay -= duration
            time.sleep(delay)
        
        
    def offset(arg1, arg2, offset):
        """
        Function takes two array-like series and aligns them based on some
        offset parameter, representing a common point.
        """
        return
    
    def interp(arg1, arg2):
        """
        Calls scipy interpolation function.
        (If we want to interpolate a position/orientation, might need to add 
        behavior to convert 4x4 tranforms to quaternions before interpolation.)
        """
        return



class Node():
    """
    Class for object creation. Not sure yet if we should accept path to mesh or 
    sequence of verticies defining a primitive.
    """
    def __init__(self):
        
        # set some current coordinate frame attributes
        self.world = np.eye(4)
        self.local = np.eye(4)

        # current position, orientation
        self.position = np.matrix([0, 0, 0])
        self.orientation = np.matrix([0, 0, 0])
        
    def setPosition(self, frame=self.world):
        """
        Updates the current position of a Node object.
        """
        self.

    def getPosition(self, time, frame=self.world):
        """
        Returns the position of an object in specified coordinate frame.
        """
        
        return


    def getEuler(self, sample):
        return

    def getQuat(self, sample):
        return

    def _transform(self, , frame="world"):
        """
        This function performs a passive transformation
        """

        return 

    def _translate(self, translation, frame=self.world):
        """
        This function performs a translation in 3D using translation matrix
        
        Arguments:
        --------
        loc: array-like of length 3
        The location to which the object will be translated in cartesian x,y,z
        
        frame: string
        The coordinate frame relative to which the translation should occur.
        This can be "local" or "world" 
        """
        Tx, Ty, Tz = translation

        # adapt position to specified coordinate frame
        currentPos = self.getPosition()
        passivePos = self._transform(frame=frame)
        
        # translate
        transMatrix = np.matrix([
                [1, 0, 0, 0],
                [0, 1, 0, 0],
                [0, 0, 1, 0],
                [Tx, Ty, Tz, 1]
                ])
        newPos = passivePos.T * transMatrix
        
        return newPos

    def _rotate(self, euler, frame=self.world):
        """
        This function performs a rotation in 3D using a translation matrix
        
        Arguments:
        --------
        euler: array-like of length 3 

        """
        
        return

    def _scale(self, coeff, coordFrame=world):
        return

    def getVelocity(self, sample):
        return

    def getAngularVelocity(self, sample):
        return


if __name__ == "__main__":

    head = Node("path/to/data/file")
    eyeLeft = Node("...")
    eyeRight = Node("...")
    
