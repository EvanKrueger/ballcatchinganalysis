
from __future__ import division
import PerformParser as pp
import pandas as pd
import numpy as np
import Quaternion as qu
import performFun as pF



def calcPaddleBasis(sDF):

    paddleDf = pd.DataFrame()
    paddleDf.index.name = 'frameNum'

    ########################################################
    # Convert quats to rotation matrices
    
    paddleRotMat_fr_mRow_mCol = [qu.Quat(np.array(q,dtype=np.float))._quat2transform() for q in sDF.paddleQuat.values]    
    np.shape(paddleRotMat_fr_mRow_mCol)

    ########################################################
    ### Calc normal of paddle face
    
    paddleForward_fr_XYZ = np.array([[0,0,-1]] * len(paddleRotMat_fr_mRow_mCol))
       
    paddleFaceNorm_fr_XYZ = np.array([ np.dot(paddleRotMat_fr_mRow_mCol[fr],paddleForward_fr_XYZ[fr]) 
         for fr in range(len(paddleForward_fr_XYZ))])

    paddleFordwardVecDf = pd.DataFrame({('paddleFaceDir','X'):paddleFaceNorm_fr_XYZ[:,0],
                                 ('paddleFaceDir','Y'):paddleFaceNorm_fr_XYZ[:,1],
                                 ('paddleFaceDir','Z'):paddleFaceNorm_fr_XYZ[:,2]})

    paddleDf = pd.concat([paddleDf,paddleFordwardVecDf],axis=1)

    ########################################################
    ### Calc paddle UP vector

    paddleUp_fr_XYZ = np.array([[0,-1,0]] * len(paddleRotMat_fr_mRow_mCol))

    paddleUpNorm_fr_XYZ = np.array([ np.dot(paddleRotMat_fr_mRow_mCol[fr],paddleUp_fr_XYZ[fr]) 
         for fr in range(len(paddleUp_fr_XYZ))])

    paddleUpVecDf = pd.DataFrame({('paddleUpDir','X'):paddleUpNorm_fr_XYZ[:,0],
                                 ('paddleUpDir','Y'):paddleUpNorm_fr_XYZ[:,1],
                                 ('paddleUpDir','Z'):paddleUpNorm_fr_XYZ[:,2]})

    paddleDf = pd.concat([paddleDf,paddleUpVecDf],axis=1)
    
    ########################################################
    ### Calc paddle face lateral vector

    paddleFaceLatVec_fr_xyz = np.cross(paddleDf['paddleFaceDir'],paddleDf['paddleUpDir'])

    paddlFaceLatVecDf = pd.DataFrame({('paddlFaceLatDir','X'):paddleFaceLatVec_fr_xyz[:,0],
                                 ('paddlFaceLatDir','Y'):paddleFaceLatVec_fr_xyz[:,1],
                                 ('paddlFaceLatDir','Z'):paddleFaceLatVec_fr_xyz[:,2]})

    paddleDf = pd.concat([paddleDf,paddlFaceLatVecDf],axis=1)
    paddleDf[1:10]
    
    ########################################################
    ### Calc paddle-to-ball vector

    paddleToBallVec_fr_XYZ = sDF['ballPos'].values - sDF['paddlePos'].values

    paddleToBallVecDf = pd.DataFrame({('paddleToBallVec','X'):paddleToBallVec_fr_XYZ[:,0],
                                 ('paddleToBallVec','Y'):paddleToBallVec_fr_XYZ[:,1],
                                 ('paddleToBallVec','Z'):paddleToBallVec_fr_XYZ[:,2]})
                 
    paddleDf = pd.concat([paddleDf,paddleToBallVecDf],axis=1)

    ########################################################
    ### Calc paddle-to-ball direction (normalized paddle-to-ball vector)

    paddleToBallDir_fr_XYZ = np.array([np.divide(XYZ,np.linalg.norm(XYZ)) for XYZ in paddleToBallVec_fr_XYZ],dtype=np.float)

    paddleToBallDirDf = pd.DataFrame({('paddleToBallDir','X'):paddleToBallDir_fr_XYZ[:,0],
                                 ('paddleToBallDir','Y'):paddleToBallDir_fr_XYZ[:,1],
                                 ('paddleToBallDir','Z'):paddleToBallDir_fr_XYZ[:,2]})
                 
    paddleDf = pd.concat([paddleDf,paddleToBallDirDf],axis=1)

    #########################################################
    #### Calc paddle-to-ball direction XZ

    paddleToBallVec_fr = paddleToBallDirDf.values
    paddleToBallVec_fr[:,1] = 0
    paddleToBallVec_fr = pd.DataFrame(paddleToBallVec_fr)
    paddleToBallVec_fr
    
    paddleToBallVecXZDf = paddleToBallVec_fr.rename(columns={0: ('paddleToBallDirXZ','X'), 1:('paddleToBallDirXZ','Y'), 2: ('paddleToBallDirXZ','Z')})

    paddleToBallDirXZDf = paddleToBallVecXZDf.apply(lambda x: np.divide(x,np.linalg.norm(x)),axis=1)

    paddleDf = pd.concat([paddleDf,paddleToBallDirXZDf],axis=1,verify_integrity=True)

    #########################################################
    #### Calc paddle-to-ball lateral direction XZ

    paddleToBallLatVecXZ_fr = np.cross(paddleToBallDirXZDf,[[0,1,0]]*len(paddleToBallDirXZDf))

    paddleToBallLatVecDf = pd.DataFrame({('paddleToBallLatDirXZ','X'):paddleToBallLatVecXZ_fr[:,0],
    	('paddleToBallLatDirXZ','Y'):paddleToBallLatVecXZ_fr[:,1],
    	('paddleToBallLatDirXZ','Z'):paddleToBallLatVecXZ_fr[:,2]})

    paddleDf = pd.concat([paddleDf,paddleToBallLatVecDf],axis=1)

    # #########################################################
    # # ### Calc paddle-to-ball direction (normalized paddle-to-ball vector)

    # # paddleToBallVec_fr = paddleToBallDirDf.values
    # # paddleToBallVec_fr[:,1] = 0
    # # paddleToBallVec_fr = pd.DataFrame(paddleToBallVec_fr)
    # # paddleToBallVec_fr
    # # paddleToBallDirXZDf = ballTrajDir_XYZ.rename(columns={0: ('paddleToBallDirXZ','X'), 1:('paddleToBallDirXZ','Y'), 2: ('paddleToBallDirXZ','Z')})
    # # paddleToBallDirXZDf = paddleToBallDirXZDf.apply(lambda x: np.divide(x,np.linalg.norm(x)),axis=1)

    # # paddleDf = pd.concat([paddleDf,paddleToBallDirXZDf],axis=1)

    return paddleDf




def initTrialInfo(sessionDf):
    '''
    Create a trial info dataframe with some basic settings
    
    '''

    postBlankDur_tr = []
    preBlankDur_tr = []
    blankDur_tr = []
    
    gbTrials = sessionDf.groupby('trialNumber')

    for trNum, tr in gbTrials:

        preBlankDur_tr.append(tr['preBlankDur'].values[0])
        blankDur_tr.append(tr['blankDur'].values[0])
        postBlankDur_tr.append(tr['postBlankDur'].values[0])


    trialInfo = pd.DataFrame({('preBlankDur',''):preBlankDur_tr,
                       ('blankDur',''):blankDur_tr,
                       ('postBlankDur',''):postBlankDur_tr})
    
    trialInfo['firstFrame'] = [tr.index[0] for trNum, tr in gbTrials]
    trialInfo['lastFrame'] = [tr.index[-1] for trNum, tr in gbTrials]

    trialInfo = calcTrialOutcome(sessionDf,trialInfo)
    trialInfo.index.name = 'trialNum'


    return trialInfo



def initProcessedData(sessionDf):

     procData =   pd.DataFrame({
        ('trialNumber',''):sessionDf.trialNumber,
        ('frameTime',''):sessionDf.frameTime,
        ('eventFlag',''): sessionDf.eventFlag})

     procData.index.name = 'frameNum'

     return procData



def calcTrialOutcome(sessionDf,sessionTrialInfo):
    '''
    Accepts: 
        session dataFrame
        trialInfo dataFrame
    Returns: 
        trialInfo dataFrame
    '''
    gbTrials = sessionDf.groupby('trialNumber')


    ballCaughtQ = []
    ballCaughtFr = []

    for trNum, tr in gbTrials:
        
        catchFr = pF.findFirst(tr['eventFlag'],'ballOnPaddle')
        
        if( catchFr ):

            ballCaughtQ.append(True)
            ballCaughtFr.append(tr.index.values[catchFr])        
            
        else:
            ballCaughtQ.append(False)
            ballCaughtFr.append(np.nan)

    df = pd.DataFrame({('ballCaughtFr',''):ballCaughtFr,('ballCaughtQ',''):ballCaughtQ})

    return pd.concat([df,sessionTrialInfo],axis=1)

